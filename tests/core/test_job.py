import unittest
from unittest.mock import patch, MagicMock

from cincoconfig import Schema
from pydantic import ValidationError

from provisioner.core.jobs import Job, JobStatus
from provisioner.structs.plugins import Plugin
from provisioner.structs.options import Option, MultiOption, BoolOption, IntOption


class TestJob(unittest.TestCase):
    @patch('provisioner.ui.cli.shell.MongoDB')
    @patch('sys.exit')
    @patch('provisioner.ui.cli.shell.ProvisionerShell.cmdloop')
    def setUp(self, mock_shell: MagicMock, mock_exit: MagicMock, mock_mongo: MagicMock):
        altitude = IntOption(name='altitude', val=10000, oid='altitude')  # mock YML loader adding altitude
        self.config = Schema()()
        self.plugin = Plugin(name='Test Plugin', id='test_plugin', usage={}, options=[
            Option(name='Option One', oid='option_one'),
            Option(name='Option Two', oid='option_two'),
            altitude,
        ])
        self.plugin2 = Plugin(name='Test Plugin 2', id='test_plugin2', usage={}, options=[
            Option(name='Option Three', oid='option_three'),
            Option(name='Option Four', oid='option_four'),
            altitude,
        ])
        # Every yaml plugin will at least have an altitude option. Currently no PY plugins that are
        # standalone, and no current intention or engineering to do that
        self.no_options_plugin = Plugin(name='Test Plugin 3', id='test_plugin3', usage={}, options=[altitude])

    def test_get_default_option_config(self):
        assert Job(config=self.config).options == Job.get_base_configuration(self.config)

    def test_job_statuses(self):
        job = Job(config=self.config)
        # Job Status should default to Pending
        assert job.status == JobStatus.Pending.value
        # Job status can be set by string
        job.status = "running"
        assert job.status == JobStatus.Running.value
        # Job status can be set by enum
        job.status = JobStatus.Successful
        assert job.status == 'successful'
        # status is converted to string representation
        assert job.dict()['status'] == 'successful'
        # Invalid Status will Raise
        with self.assertRaises(ValidationError):
            job.status = "invalid_status"

    def test_job_is_done(self):
        # Check is done
        for status in JobStatus:
            if status in (JobStatus.Successful, JobStatus.Failed):
                assert Job(config=self.config, status=status).done is True, f"Job was not complete with status {status}"
            else:
                assert Job(config=self.config, status=status).done is False, f"Job was complete with status {status}"

    def test_config_not_in_job_dict(self):
        assert 'config' not in Job(config=self.config).dict()

    def test_to_from_json(self):
        job = Job(config=self.config)
        plugins = job.options.resolve_config('plugins')
        plugins.add_plugin(self.plugin, 'asdf')
        subconf = job.options.resolve_config(f'plugins.{self.plugin.id}:asdf')
        subconf.add_plugin(self.plugin2, 'fdas')
        plugins.options.append(MultiOption(
            oid='oid',
            pkey=Option(oid='pkey', name='PKey'),
            options=[
                Option(oid='oid', name='T', value='blah'),
                BoolOption(oid='oid', name='T', value=True),
            ]
        ))
        assert job == Job.parse_obj(job.dict())
        assert job == Job.parse_raw(job.json())
