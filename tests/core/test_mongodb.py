from unittest import TestCase
from unittest.mock import patch
from provisioner.core.mongodb import MongoDB
from provisioner.config import Schema


class TestMongoDB(TestCase):
    @patch('provisioner.core.mongodb.MongoClient')
    def setUp(self, mock_mongo):
        config = Schema()
        config.database.host = 'MockHost'
        config.database.port = 1010
        config.database.user = "MockUser"
        config.database.password = "MockPassword"
        self.mongo = MongoDB(config)

    @patch("provisioner.core.mongodb.warning")
    def test_save_deployed_vm(self, mock_warn):
        # Test only valid data passed is saved in DB
        self.mongo.save_deployed_vm({"error": "no valid VM config supplied"})
        mock_warn.assert_called()
        self.mongo.db.vm_templates.insert_one.assert_not_called()

        # Test valid data parsed correctly
        mock_warn.reset_mock()
        self.mongo.save_deployed_vm({"name": "TestVM", "OS": "Linux duh"})

        mock_warn.assert_not_called()
        self.mongo.db.vm_templates.insert_one.assert_called()
