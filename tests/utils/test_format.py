from pathlib import Path, PosixPath
import unittest
from unittest.mock import patch, MagicMock

from provisioner.utils.format import shorten_home_path

class TestOptions(unittest.TestCase):
    def setUp(self):
        self.homedir = PosixPath('/fake/home')

    @patch('pathlib.Path.home')
    def test_path_containing_home(self, mock_home: MagicMock):
        mock_home.return_value = self.homedir
        p = self.homedir / 'foo/bar'
        p = shorten_home_path(p)
        assert str(p).startswith('~/')

    @patch('pathlib.Path.home')
    def test_path_not_containing_home(self, mock_home: MagicMock):
        mock_home.return_value = self.homedir
        p = Path('/foo/bar')
        n = shorten_home_path(p)
        assert p == n
