import sys
import unittest
from io import StringIO
from unittest.mock import MagicMock

from pydantic import ValidationError, parse_obj_as

from provisioner.structs.options import (
    Option, IntOption, BoolOption, ListOption, PortOption, HostOption, TargetList,
    CronTimeMinuteOption, CronTimeHourOption, CronTimeDayOption, CronTimeMonthOption,
    CronTimeDOTWOption, AnyOption, MultiOption, SetOption
)


class TestOptions(unittest.TestCase):
    def test_option_to_from_json(self):
        opts = [
            Option(oid='oid', name='T', value='blah'),
            BoolOption(oid='oid', name='T', value=True),
            IntOption(oid='oid', name='T', value=69420),
            ListOption(oid='oid', name='T', value=['test', 69]),
            TargetList(oid='oid', name='T', value=['1.1.1.1', 'hostname']),
            HostOption(oid='oid', name='T', value='1.1.1.1'),
            PortOption(oid='oid', name='T', value=4444, privileged_ok=False),
            PortOption(oid='oid', name='T', value=80, privileged_ok=True),
            # ErrorAction(oid='oid', name='T'),
            # AnsibleHostsOption(oid='oid', name='T'),
            # CronFileOption(oid='oid', name='T'),
        ]
        for opt in opts:
            assert opt == parse_obj_as(AnyOption, opt.dict()), f"Option failed with type=value {opt.op_type}={opt.tree()}"

    def test_multi_option_to_from_json(self):
        option = MultiOption(
            oid='oid',
            pkey=Option(oid='pkey', name='PKey'),
            options=[
                Option(oid='oid', name='T', value='blah'),
                BoolOption(oid='oid', name='T', value=True),
            ]
        )
        assert option == AnyOption.parse_obj(option.dict())
        assert option == AnyOption.parse_raw(option.json())

    def test_option_with_range_to_json(self):
        option = IntOption(name="Ansible Verbosity", oid='verbosity', choices=range(0, 5))
        assert option == Option.parse_obj(option.dict())
        assert option == Option.parse_raw(option.json())

    def test_option_to_from_tree(self):
        opts = [
            Option(oid='oid', name='T', value='blah'),
            BoolOption(oid='oid', name='T', value=True),
            IntOption(oid='oid', name='T', value=69420),
            ListOption(oid='oid', name='T', value=['test', 69]),
            TargetList(oid='oid', name='T', value=['1.1.1.1', 'hostname']),
            HostOption(oid='oid', name='T', value='1.1.1.1'),
            PortOption(oid='oid', name='T', value=4444, privileged_ok=False),
            PortOption(oid='oid', name='T', value=80, privileged_ok=True),
            # ErrorAction(oid='oid', name='T'),
            # AnsibleHostsOption(oid='oid', name='T'),
            # CronFileOption(oid='oid', name='T'),
        ]
        for old in opts:
            new = old()
            new.reset()  # reset to the default value
            assert new.val != old.val
            new.from_tree(old.val)
            assert old == new, f"Option failed with type=value {old.op_type}={old.tree()}"

    def test_multi_option_to_from_tree_clone_option(self):
        option = MultiOption(
            oid='multi_option',
            pkey=Option(oid='pkey', name='PKey'),
            options=[
                Option(oid='option_oid', name='Option', value='blah'),
                BoolOption(oid='bool_oid', name='Bool', value=True),
            ]
        )
        # clone the option before we add values
        new = option()
        assert option == new  # sanity check

        # add some values
        pkey1, opts1 = option.add_pkey('pkey1')
        pkey2, opts2 = option.add_pkey('pkey2')
        opts1[0].val = 'opt1'
        opts1[1].val = False
        opts2[0].val = 'opt2'
        opts2[1].val = True

        assert option != new  # sanity check, values should now differ

        # run from_tree to copy values
        new.from_tree(option.tree())

        # did it work? should be exactly the same now
        assert option == new

    def test_multi_option_to_from_tree_change_option(self):
        """
        This test represents us loading a job multioption from the database, when a plugin's options
        have changed. The name and default values change, but the option IDs do not.
        The new option also adds in some new values
        """
        # setup options
        option = MultiOption(
            oid='multi_option',
            pkey=Option(oid='pkey', name='PKey'),
            options=[
                Option(oid='option_oid', name='Option', value='blah'),
                PortOption(oid='port_oid', name='Port', value=4444),
            ]
        )
        new = MultiOption(
            oid='multi_option',
            pkey=Option(oid='pkey', name='New PKey'),
            options=[
                Option(oid='option_oid', name='New Option', value='blah'),
                PortOption(oid='port_oid', name='New Port', value=4444),
                BoolOption(oid='bool_oid', name='New Bool', value=True),
            ]
        )

        # set some values
        pkey1, opts1 = option.add_pkey('pkey1')
        pkey2, opts2 = option.add_pkey('pkey2')
        opts1[0].val = 'opt1'  # string option 1
        opts1[1].val = 5555    # port option 1
        opts2[0].val = 'opt2'  # string option 2
        opts2[1].val = 6666    # port option 2

        # run from_tree to copy values
        new.from_tree(option.tree())

        # options won't be equal here, check the fields we expect to match
        str_opt_1: Option = new.get_option('[pkey1].option_oid')
        port_opt_1: Option = new.get_option('[pkey1].port_oid')
        bool_opt_1: Option = new.get_option('[pkey1].bool_oid')
        assert str_opt_1.name == "New Option"
        assert str_opt_1.val == "opt1"
        assert port_opt_1.name == "New Port"
        assert port_opt_1.val == 5555
        assert bool_opt_1.name == "New Bool"  # just verifying it exists
        str_opt_2: Option = new.get_option('[pkey2].option_oid')
        port_opt_2: Option = new.get_option('[pkey2].port_oid')
        bool_opt_2: Option = new.get_option('[pkey2].bool_oid')
        assert str_opt_2.name == "New Option"
        assert str_opt_2.val == "opt2"
        assert port_opt_2.name == "New Port"
        assert port_opt_2.val == 6666
        assert bool_opt_2.name == "New Bool"  # just verifying it exists

    def test_multi_option_from_tree_prints_warning_on_missing_oid(self):
        """
        This test represents when we load a job multioption from the DB, and a plugin option no longer exists
        """
        # setup options
        option = MultiOption(
            oid='multi_option',
            pkey=Option(oid='pkey', name='PKey'),
            options=[
                Option(oid='option_oid', name='Option', value='blah'),
                PortOption(oid='port_oid', name='Port', value=4444),
            ]
        )
        new = MultiOption(
            oid='multi_option',
            pkey=Option(oid='pkey', name='New PKey'),
            options=[
                Option(oid='option_oid', name='New Option', value='blah'),
                BoolOption(oid='bool_oid', name='New Bool', value=True),
            ]
        )
        # set some values
        pkey1, opts1 = option.add_pkey('pkey1')
        opts1[0].val = 'opt1'  # string option 1
        opts1[1].val = 5555    # port option 1

        # run from_tree to copy values
        captured_output = StringIO()  # Create StringIO object
        _stdout = sys.stdout
        sys.stdout = captured_output  # and redirect stdout.
        try:
            new.from_tree(option.tree())
        finally:
            sys.stdout = _stdout
        out = captured_output.getvalue()
        assert out.startswith("failed to set value")
        assert "5555" in out, "value not printed in warning"

    def test_oid_validator(self):
        with self.assertRaises(ValidationError):
            Option(oid='invalid.oid', name='T')

        option = Option(oid='valid_oid', name='T')
        with self.assertRaises(ValidationError):
            option.oid = 'oid.with.dots'

    def oidions_get_default_value(self):
        option = Option(oid='oid', name='T', default='_default')
        assert option.val == '_default'
        option = Option(oid='oid', name='T', default=lambda: '_default')
        assert option.val == '_default'

    def test_invalid_default_raises(self):
        with self.assertRaises(ValidationError):
            IntOption(oid='oid', name='T', default_val=lambda: '_default')

    def test_choices_is_list_validator(self):
        for val in (['test1'], [1], [1, 2], range(0, 5)):
            assert Option(oid='oid', name='T', choices=val).choices == val, f"{val} failed as valid choices"
        # Special case, None is converted to empty list
        assert Option(oid='oid', name='T', choices=None).choices == []
        # No value is also an empty list
        assert Option(oid='oid', name='T').choices == []
        with self.assertRaises(ValidationError):
            Option(oid='oid', name='T', choices=1)
        with self.assertRaises(ValidationError):
            Option(oid='oid', name='T', choices='one')

    def test_choices_are_valid(self):
        with self.assertRaises(ValidationError):
            IntOption(oid='oid', name='T', choices=['one'])

    def test_int_0_is_allowed_when_required(self):
        option = IntOption(oid='oid', name='T', value=0, required=True)
        assert option.val == 0
        assert option.check(MagicMock(), MagicMock()) == []

    def test_parse_int(self):
        assert IntOption(oid='oid', name='T', value=1).val == 1
        assert IntOption(oid='oid', name='T', value='1').val == 1
        # Confirm a None value is fine
        assert IntOption(oid='oid', name='T').val is None
        assert IntOption(oid='oid', name='T', value=None).val is None

    def test_parse_bool(self):
        for val in ('yes', 'y', 'true'):
            assert BoolOption(oid='oid', name='T', value=val).val is True, f"{val} failed to parse as True"
        for val in ('no', 'n', 'false'):
            assert BoolOption(oid='oid', name='T', value=val).val is False, f"{val} failed to parse as False"
        with self.assertRaises(ValidationError):
            BoolOption(oid='oid', name='T', value='ferp')

    def test_port_option(self):
        # Non-priv ports are okay
        assert PortOption(name='T', oid='test', value=1025, privileged_ok=True).val == 1025
        assert PortOption(name='T', oid='test', value=1025, privileged_ok=False).val == 1025
        # Priv-port and privileged_ok=True
        assert PortOption(name='T', oid='test', value=80, privileged_ok=True).val == 80
        # Priv-port and privileged_ok=False
        with self.assertRaises(ValueError):
            PortOption(name='T', oid='test', value=80, privileged_ok=False)
        # Lower Limit
        assert PortOption(name='T', oid='test', value=1, privileged_ok=True).val == 1
        with self.assertRaises(ValueError):
            PortOption(name='T', oid='test', value=0, privileged_ok=True)
        # Upper Limit
        assert PortOption(name='T', oid='test', value=65535).val == 65535
        with self.assertRaises(ValueError):
            PortOption(name='T', oid='test', value=65536)

    def test_host_option(self):
        for val in ('192.168.1.1', 'hostname', 'hostname.domain', 'hostname.extended.domain'):
            assert HostOption(oid='oid', name='T', value=val).val == val, f"{val} failed as valid value"
        with self.assertRaises(ValueError):
            HostOption(name='T', oid='test', value='a' * 256)
        # Value doesn't match regex
        with self.assertRaises(ValueError):
            HostOption(name='T', oid='test', value='test_host')
        with self.assertRaises(ValueError):
            HostOption(name='T', oid='test', value='256.256.256.$')

    def test_target_list_option(self):
        for val in ({'192.168.1.1'}, {'hostname'}, {'hostname.domain'}, {'hostname.extended.domain'}):
            assert TargetList(oid='oid', name='T', value=val).val == val, f"{val} failed as valid value"
        # Value too long
        with self.assertRaises(ValueError):
            TargetList(name='T', oid='test', value={'a' * 256})
        # Value doesn't match regex
        with self.assertRaises(ValueError):
            TargetList(name='T', oid='test', value={'test_host'})
        # test by passing set
        option = TargetList(
            name='T', oid='test', value={'192.168.1.1', 'hostname', 'hostname.domain', 'hostname.extended.domain'}
        )
        for val in ('192.168.1.1', 'hostname', 'hostname.domain', 'hostname.extended.domain'):
            assert val in option.val, f"{val} not found in TargetList"
        # test by passing list
        option = TargetList(
            name='T', oid='test', value=['192.168.1.1', 'hostname', 'hostname.domain', 'hostname.extended.domain']
        )
        for val in ('192.168.1.1', 'hostname', 'hostname.domain', 'hostname.extended.domain'):
            assert val in option.val, f"{val} not found in TargetList"
        # test by passing strings
        option = TargetList(
            name='T', oid='test', value='192.168.1.1'
        )
        option.add('hostname')
        option.add('hostname.domain')
        option.add('hostname.extended.domain')
        for val in ('192.168.1.1', 'hostname', 'hostname.domain', 'hostname.extended.domain'):
            assert val in option.val, f"{val} not found in TargetList"

    def test_list_option(self):
        # Test parsing val by setting via __init__
        for val in ('a', 'b', ['a', 'b']):
            assert ListOption(oid='oid', name='T', choices=['a', 'b', 'c'], value=val).val == list(val),\
                f"{val} failed to parse as a List"
        # Test parsing val by setting on an already instantiated option
        option = ListOption(oid='oid', name='T', choices=['a', 'b', 'c'])
        assert option.val == []
        option.val = 'a'
        assert option.val == ['a']
        option.val = 'b'
        assert option.val == ['b']
        option.val = ['a', 'b']
        assert option.val == ['a', 'b']
        # Test that values are validated against choices
        with self.assertRaises(ValidationError):
            option.val = 'd'
        with self.assertRaises(ValidationError):
            option.val = ['d']
        # Test that values can be appended/extended
        option = ListOption(oid='oid', name='T', choices=['a', 'b', 'c'])
        assert option.val == []
        option.append('a')
        assert option.val == ['a']
        option.append('b')
        assert option.val == ['a', 'b']
        option = ListOption(oid='oid', name='T', choices=['a', 'b', 'c'])
        assert option.val == []
        option.extend(['a'])
        assert option.val == ['a']
        option.extend(['b', 'c'])
        assert option.val == ['a', 'b', 'c']
        # Test that values are validated when appending and extending the list
        with self.assertRaises(ValidationError):
            option.append('d')
        with self.assertRaises(ValidationError):
            option.extend(['c', 'd', 'e'])

    def test_crontime_option(self):
        # Test invalid minute values
        for val in (-1, 60, 'test', True, '*/74', range(0, 20)):
            with self.assertRaises(ValidationError):
                CronTimeMinuteOption(oid='minute', name='minute', value=val)
        # Test valid minute values
        for val in (0, '10', 59, '*', '*/30', None):
            CronTimeMinuteOption(oid='minute', name='minute', value=val)

        # Test invalid hour values
        for val in ('-1', 24, 'test', True, '*/60'):
            with self.assertRaises(ValidationError):
                CronTimeHourOption(oid='hour', name='hour', value=val)
        # Test valid hour values
        for val in ('0', 10, '23', '*', '*/2', None):
            CronTimeHourOption(oid='hour', name='hour', value=val)

        # Test invalid month values
        for val in ('-1', 0, 13, 'test', True, '*/60'):
            with self.assertRaises(ValidationError):
                CronTimeMonthOption(oid='month', name='month', value=val)
        # Test valid month values
        for val in ('jan', 'sep', '*', None):
            CronTimeMonthOption(oid='month', name='month', value=val)

        # Test invalid dayofweek values
        for val in ('-1', 0, 13, 'test', True, '*/60'):
            with self.assertRaises(ValidationError):
                CronTimeDOTWOption(oid='day_of_week', name='day of the week', value=val)
        # Test valid dayofweek values
        for val in ('sun', 'tue', '*', None):
            CronTimeDOTWOption(oid='day_of_week', name='day of the week', value=val)

    def test_iter_option_add_del(self):
        """
        Test __delitem__ and __iadd__ features of _IterOption
        """
        for clazz in (ListOption, SetOption):
            opt = clazz(oid='oid', name='T', val=['a'])
            assert set(opt.val) == {'a'}  # use set for checks so the order doesn't matter
            opt += 'b'
            assert set(opt.val) == {'a', 'b'}
            opt += {'c', 'd'}
            assert set(opt.val) == {'a', 'b', 'c', 'd'}
            opt += {'e', 'f'}
            assert set(opt.val) == {'a', 'b', 'c', 'd', 'e', 'f'}
            del opt['f']
            assert set(opt.val) == {'a', 'b', 'c', 'd', 'e'}
