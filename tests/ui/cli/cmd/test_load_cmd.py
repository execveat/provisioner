import unittest
from pathlib import Path
from unittest.mock import patch, MagicMock

from provisioner.ui.cli.shell import ProvisionerShell


class TestShell(unittest.TestCase):
    @patch('provisioner.ui.cli.shell.MongoDB')
    @patch('sys.exit')
    @patch('provisioner.ui.cli.shell.ProvisionerShell.cmdloop')
    def setUp(self, mock_shell: MagicMock, mock_exit: MagicMock, mock_mongo: MagicMock):
        mock_shell.return_value = 0
        self.shell = ProvisionerShell()

    @patch('provisioner.loaders.python.PythonPluginLoader.load_plugin')
    @patch('provisioner.loaders.python.PythonPluginLoader.find_plugins')
    @patch('pathlib.Path.exists')
    def test_load_py_plugin(self, mock_path: MagicMock, mock_find_plugins: MagicMock, mock_load_plugin: MagicMock):
        mock_path.return_value = True

        path = Path('plugins/persistence/at.py')
        self.assertEqual(0, self.shell.load_command.load_plugins(self.shell, path))
        mock_load_plugin.assert_called_with(path, name=None)

        self.assertEqual(0, self.shell.load_command.load_plugins(self.shell, path, python=True))
        mock_load_plugin.assert_called_with(path, name=None)

        self.assertEqual(0, self.shell.load_command.load_plugins(self.shell, path, name="test"))
        mock_load_plugin.assert_called_with(path, name="test")

        path = Path('plugins/')
        self.assertEqual(2, self.shell.load_command.load_plugins(self.shell, path, recursive=True))

        self.assertEqual(0, self.shell.load_command.load_plugins(self.shell, path, recursive=True, python=True))
        mock_find_plugins.assert_called_with(path)

    @patch('provisioner.loaders.yaml.YamlPluginLoader.load_plugin')
    @patch('provisioner.loaders.yaml.YamlPluginLoader.find_plugins')
    @patch('pathlib.Path.exists')
    def test_load_yml_plugin(self, mock_path: MagicMock, mock_find_plugins: MagicMock, mock_load_plugin: MagicMock):
        mock_path.return_value = True

        path = Path('plugins/persistence/c2games.yaml')
        self.assertEqual(0, self.shell.load_command.load_plugins(self.shell, path))
        mock_load_plugin.assert_called_with(path, name=None)

        self.assertEqual(0, self.shell.load_command.load_plugins(self.shell, path, yaml=True))
        mock_load_plugin.assert_called_with(path, name=None)

        self.assertEqual(0, self.shell.load_command.load_plugins(self.shell, path, name="test"))
        mock_load_plugin.assert_called_with(path, name="test")

        path = Path('plugins/')
        self.assertEqual(2, self.shell.load_command.load_plugins(self.shell, path, recursive=True))

        self.assertEqual(0, self.shell.load_command.load_plugins(self.shell, path, recursive=True, yaml=True))
        mock_find_plugins.assert_called_with(path)

    @patch('pathlib.Path.exists')
    def test_nonexistent_path(self, mock_path: MagicMock):
        mock_path.return_value = False
        path = Path('plugins/persistence/at.py')
        self.assertEqual(3, self.shell.load_command.load_plugins(self.shell, path, recursive=True))

    @patch('provisioner.ui.cli.cmd.load.LoadCommand.unload_plugin')
    @patch('provisioner.ui.cli.cmd.load.LoadCommand.load_plugins')
    def test_parse_args(self, mock_load_plugins: MagicMock, mock_unload_plugin: MagicMock):
        mock_load_plugins.return_value = 0
        mock_unload_plugin.return_value = 0
        self.assertEqual(1, self.shell.load_command.run(self.shell, ['--python', '--yaml', 'some/path']))
        self.assertEqual(0, self.shell.load_command.run(self.shell, ['--remove', 'plugin']))
        mock_unload_plugin.assert_called_once()

        self.assertEqual(1, self.shell.load_command.run(self.shell, ['--asdf', 'some/path']))
        self.assertEqual(0, self.shell.load_command.run(self.shell, ['--python', 'some/path']))

    def test_unload_plugins(self):
        self.shell.ctx.plugin_store.plugins = {"MockPlugin": {"mock": "c2games", "plugin": "info"}}
        self.assertEqual(3, self.shell.load_command.unload_plugin(self.shell, "Some Unloaded Plugin"))
        self.assertEqual(self.shell.ctx.plugin_store.plugins, {"MockPlugin": {"mock": "c2games", "plugin": "info"}})
        self.assertEqual(0, self.shell.load_command.unload_plugin(self.shell, "MockPlugin"))
        self.assertEqual(self.shell.ctx.plugin_store.plugins, {})
