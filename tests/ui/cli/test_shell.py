import unittest
from pathlib import Path
from unittest.mock import patch, MagicMock, Mock

from provisioner.ui.cli.shell import ProvisionerShell


# pass Mock() because we just want to ignore calls to touch()
@patch('pathlib.Path.touch', Mock())
class TestShell(unittest.TestCase):
    @patch('provisioner.ui.cli.shell.MongoDB')
    @patch('sys.exit')
    @patch('provisioner.ui.cli.shell.ProvisionerShell.cmdloop')
    def setUp(self, mock_shell: MagicMock, mock_exit: MagicMock, mock_mongo: MagicMock):
        mock_shell.return_value = 0
        self.shell = ProvisionerShell()

    @patch('pypsi.plugins.history.HistoryCommand.run')
    def test_shell_on_cmdloop_begin(self, mock_hist: MagicMock):
        self.shell.on_cmdloop_begin()
        mock_hist.assert_called_with(shell=self.shell, args=['load', str(Path(Path.home(), '.provisioner_history'))])

    @patch('pypsi.plugins.history.HistoryCommand.run')
    def test_shell_on_cmdloop_end(self, mock_hist: MagicMock):
        self.shell.on_cmdloop_end()
        mock_hist.assert_called_with(shell=self.shell, args=['save', str(Path(Path.home(), '.provisioner_history'))])

    def test_get_command_name_completions(self):
        ret = self.shell.get_command_name_completions('us')
        assert 'use' in ret
