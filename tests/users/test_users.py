import unittest
from unittest.mock import patch, MagicMock

from provisioner.structs.users import UserList, User


class TestUserLists(unittest.TestCase):

    # TODO: expand upon when more user verification added
    def test_add_user(self):
        # Test username required
        mock_list = UserList('mock', MagicMock())
        with self.assertRaises(KeyError):
            mock_list.add_user({"full_name": "MockName", "tag": "Mock"})

        self.assertEqual(mock_list.users, [])

        # Test new user added
        mock_list.add_user({"user_list": "mock", "username": "Mock", "tags": ['redteam', 'mockin']})
        self.assertEqual(len(mock_list.users), 1)
        self.assertEqual(mock_list.users[0].username, "Mock")

    def test_search_users(self):
        # Test kwargs stripped of None/Faulty values
        mock_db = MagicMock()
        user_list = UserList('mock', mock_db)
        user_list.search_users(stuff=None, wow=True, thing=False)
        mock_db.db.user_db.users.find.assert_called_with({'wow': True, 'user_list': 'mock', 'user_list': 'mock'})

        # Test groups/tags kwargs properly formatted for mongo
        user_list.search_users(groups=['mock1', 'mock2'])
        mock_db.db.user_db.users.find.assert_called_with({'groups': {'$in': ['mock1', 'mock2']}, 'user_list': 'mock'})

        user_list.search_users(tags=['tag1', 'tag2'])
        mock_db.db.user_db.users.find.assert_called_with({'tags': {'$in': ['tag1', 'tag2']}, 'user_list': 'mock'})
