from pathlib import Path

import yaml
from cincoconfig import Schema

from provisioner.core.jobs import Job
from provisioner.structs.plugins import C2GamesMeta, RecursiveConfig


def load_yaml_plugin(plug_id: str, contents: dict, path: Path):
    return C2GamesMeta(**contents, id=plug_id, source=path, source_dir=path.parent, usage={
        'include_role': {'name': plug_id}
    })


def get_base_plugin_config() -> RecursiveConfig:
    return Job.get_base_configuration(Schema()())


TEST_YAML_NFS = open(Path(Path(__file__).parent.parent, 'plugins', 'yaml_plugs', 'nfs', 'c2games.yml')).read()

TEST_YAML_NFS_DICT = yaml.safe_load(TEST_YAML_NFS)
TEST_YAML_NFS_DICT['Options']['altitude'] = {  # when loading yaml plugins, altitude option is added
    "name": "altitude", "type": "int", "default": "10000", "oid": "altitude",
    "description": "Decides when a plugin will run. Lower value runs first."
}
TEST_YAML_NFS_PLUGIN = load_yaml_plugin('nfs', TEST_YAML_NFS_DICT, Path('nfs'))
