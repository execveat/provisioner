from typing import List, Dict

from provisioner.structs.plugins import Plugin


class Cron(Plugin):
    provides: List[str] = ['persistence', 'linux.persistence']
    usage: Dict = {}


__plugin__ = Cron
