---
Title: 'Malicious Authorized Keys'
Version: 1.0
Author: 'Brodie Davis'
Organization: 'C2 Games'

Type: 'Misconfiguration'

Difficulty: 4

Description: >
  Installs malicious SSH keys on the system for remote access to the root user

OSCompatibility:
  - Family: Linux
    Name: Ubuntu
    Version: 18.04
  - Family: Linux
    Name: CentOS
    Version: 7

Exploitation:
  - Example: ssh -i malicious_key root@192.168.1.1
    Description: >
      SSH As root into the System to escalate privileges
  - Example: ssh -i malicious_key user@192.168.1.1 cat /etc/passwd
    Description: >
      SSH as a normal user into the system and run a single command
      to achieve remote code execution without a persistent session

Mitigation: >
  Remove the Malicious SSH Keys.

Training: >
  An exercise explaining SSH keys, and their ability to provide remote access
  into to a system. The exercise should also review the SSHD service
  configuration file, and options that may be used to secure the SSHD service.

Justification: >
  This challenge covers the skills required to configure and
  secure an OpenSSH service. The student must demonstrate that they
  can identify invalid or insecure configurations within an existing
  environment, and reconfigure the service to a functional and secure state.
