import logging
import os
from pathlib import Path
from typing import Optional, List, Union

from provisioner.deploy import SiteDeployment
from provisioner.structs.plugins import Plugin
from provisioner.structs.plugins.store import PluginStore


class PluginLoader:
    """
    Generic class to load a single plugin file or multiple plugins recursively from a directory.

    Should be inherited, and subclass must implement load_plugin for it's file type
    """
    PLUGIN_SUFFIXES: List[str] = []

    def __init__(self,
                 plugin_store: PluginStore,
                 logger: logging.Logger = None,
                 plugin_basedir: Union[Path, str] = None):
        """
        Setup Plugin Loader
        :param plugin_store: [REQUIRED] PluginStore instance to use.
        :param logger: Logger to use for messages
        :param plugin_basedir: Base directory for generating plugin names
        """
        self.logger = logger or logging.getLogger(__name__)
        self._plugin_store: PluginStore = plugin_store
        self.plugin_basedir = plugin_basedir or SiteDeployment.plugin_path

    @property
    def plugin_store(self) -> PluginStore:
        """
        Get Plugin Store used by this PluginLoader
        :return: plugin store
        """
        return self._plugin_store

    def find_plugins(self, path: Union[Path, str]) -> None:
        """
        Recursively find plugins in `path` of filetype PLUGIN_SUFFIXES
        :param path: Path to search for plugins
        """
        for cwd, _, files in os.walk(path):
            for file in files:
                # Don't load files starting with _ (special files)
                if file.startswith('_'):
                    continue
                # If PLUGIN_SUFFIXES is defined and
                # file name does not end in approved suffice, don't load
                if self.PLUGIN_SUFFIXES and len(list(filter(file.endswith, self.PLUGIN_SUFFIXES))) == 0:
                    continue

                if Path(cwd).name == SiteDeployment.config.plugins_path_ignore:
                    self.logger.debug(f"Skipping, not initializing files from {cwd}..")
                    continue

                full_path = os.path.join(cwd, file)
                self.load_plugin(full_path)

    def load_plugin(self, path: Union[Path, str], name: Optional[str] = None) -> Optional[Plugin]:
        """
        Load plugin file into PluginStore. Implemented by subclass.
        :param path: Path to file
        :param name: Optional name of plugin
        """
        raise NotImplementedError()  # pragma: no cover
