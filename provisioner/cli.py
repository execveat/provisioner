import sys
from os import path

try:
    from provisioner.ui.cli.shell import ProvisionerShell
except:  # pragma: no cover
    base_path = path.realpath(path.join(path.dirname(path.abspath(__file__)), '..'))
    sys.path.append(base_path)
    from provisioner.ui.cli.shell import ProvisionerShell


def main():
    """
    Run Provisioner Shell
    """
    shell = ProvisionerShell()
    rc = shell.cmdloop()
    sys.exit(rc)


if __name__ == '__main__':
    main()
