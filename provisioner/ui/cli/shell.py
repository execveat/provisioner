import sys
from pathlib import Path
from typing import Optional

import readline
from cincoconfig import Schema
from pypsi.namespace import Namespace
from pypsi.shell import Shell
from pypsi.plugins.cmd import CmdPlugin
from pypsi.plugins.block import BlockPlugin
from pypsi.plugins.hexcode import HexCodePlugin
from pypsi.commands.macro import MacroCommand
from pypsi.commands.system import SystemCommand
from pypsi.plugins.multiline import MultilinePlugin
from pypsi.commands.xargs import XArgsCommand
from pypsi.commands.exit import ExitCommand
from pypsi.plugins.variable import VariablePlugin, ManagedVariable
from pypsi.plugins.history import HistoryPlugin
from pypsi.plugins.alias import AliasPlugin
from pypsi.commands.echo import EchoCommand
from pypsi.commands.include import IncludeCommand
from pypsi.commands.help import HelpCommand, Topic
from pypsi.commands.tip import TipCommand
from pypsi.commands.tail import TailCommand
from pypsi.commands.chdir import ChdirCommand
from pypsi.commands.pwd import PwdCommand
from pypsi.plugins.comment import CommentPlugin
from pypsi.ansi import AnsiCodes
from pypsi import topics

from provisioner.core.mongodb import MongoDB
from provisioner.deploy import SiteDeployment
from provisioner.core.director import CLIDirector
from provisioner.structs.plugins.store import PluginStore
from provisioner.ui.cli.cmd.job import JobCommand
from provisioner.ui.cli.cmd.load import LoadCommand
from provisioner.ui.cli.cmd.use import UseCommand
from provisioner.ui.cli.cmd.set import SetCommand
from provisioner.ui.cli.cmd.deploy import DeployCommand
from provisioner.ui.cli.cmd.show import ShowCommand

from provisioner.core.jobs import Job

HIST_PATH = Path(Path.home(), '.provisioner_history')


class ProvisionerNamespace(Namespace):
    """
    Helper class to define types for typing hinting
    """
    config: Schema
    db: MongoDB
    director: CLIDirector
    plugin_store: PluginStore
    job: Job
    vars: Namespace


class ProvisionerShell(Shell):
    """
    The provisioner shell can be used to create and deploy Loadouts to VMs
    """
    ctx: ProvisionerNamespace

    # First, add commands and plugins to the shell
    echo_cmd = EchoCommand()
    block_plugin = BlockPlugin()
    hexcode_plugin = HexCodePlugin()
    macro_cmd = MacroCommand()

    # Drop commands to cmd.exe if the platform is Windows
    system_cmd = SystemCommand(use_shell=(sys.platform == 'win32'))
    ml_plugin = MultilinePlugin()
    xargs_cmd = XArgsCommand()
    exit_cmd = ExitCommand()
    history_plugin = HistoryPlugin()
    include_cmd = IncludeCommand()
    cmd_plugin = CmdPlugin(cmd_args=1)
    tip_cmd = TipCommand()
    tail_cmd = TailCommand()
    help_cmd = HelpCommand()
    var_plugin = VariablePlugin(case_sensitive=True, env=False)
    comment_plugin = CommentPlugin()
    chdir_cmd = ChdirCommand()
    pwd_cmd = PwdCommand()
    alias_plugin = AliasPlugin()

    # Custom commands
    use_command = UseCommand()
    load_command = LoadCommand()
    set_command = SetCommand()
    deploy_command = DeployCommand()
    job_command = JobCommand()
    show_command = ShowCommand()

    # noinspection PyBroadException
    # pylint: disable=bare-except
    def __init__(self):
        # You must call the Shell.__init__() method.
        super().__init__(width=160, ctx=ProvisionerNamespace())

        def get_prompt():
            return "{gray}[$time]{r} $event_prompt {cyan}provisioner{r} )>{r} ".format(
                gray=AnsiCodes.gray.prompt(), r=AnsiCodes.reset.prompt(),
                cyan=AnsiCodes.cyan.prompt(),
            )
        self.prompt = get_prompt
        self.fallback_cmd = self.system_cmd

        # Register the shell topic for the help command
        self.help_cmd.add_topic(self, Topic("shell", "Builtin Shell Commands"))
        # Add the I/O redirection topic
        self.help_cmd.add_topic(self, topics.IoRedirection)

        # todo pass cli args to config
        self.ctx.plugin_store = PluginStore()
        self.ctx.job = Job(config=SiteDeployment.config)
        self.ctx.db = MongoDB(self.ctx.job.config)
        self.ctx.vars.event_prompt = ManagedVariable(self.get_event_prompt)

        self._sys_bins = None

    def get_event_prompt(self, shell):
        """
        Get the event_id being used on the prompt at a given time
        """
        event: Optional[str] = shell.ctx.job.options.resolve_option('job', 'event_id').val
        if not event:
            return ""
        elif not sys.stdout.isatty():
            return "[%s] " % event

        txt = AnsiCodes.purple(f'[{event}]')
        if sys.stdout.isatty():
            return txt.prompt()
        return txt.s

    def on_cmdloop_begin(self):
        HIST_PATH.touch(mode=0o660)
        self.history_plugin.history_cmd.run(shell=self, args=['load', str(HIST_PATH)])
        # Attempt to automatically load YAML and Python plugins
        self.load_command.load_plugins(self, SiteDeployment.builtin_path, recursive=True, yaml=True)
        self.load_command.load_plugins(self, SiteDeployment.builtin_path, recursive=True, python=True)
        for path in SiteDeployment.plugin_path.split(':'):
            if not path:
                continue
            if Path(path).exists():
                self.load_command.load_plugins(self, path, recursive=True, yaml=True)
                self.load_command.load_plugins(self, path, recursive=True, python=True)
            else:
                print(f"Skipping invalid plugin path: {path}")

    def on_cmdloop_end(self):
        self.history_plugin.history_cmd.run(shell=self, args=['save', str(HIST_PATH)])

    def on_shell_ready(self):
        # update readline completion delimiters to not contain '-'
        # required for pypsi.completers.command_completer to work as expected on some systems
        readline.set_completer_delims("\t\n '\"\\`$=|&;<>{}")

    def get_command_name_completions(self, prefix):
        # uncomment these lines to allow tab-completion of function names
        # refs bug https://github.com/ameily/pypsi/issues/51
        # if not self._sys_bins:
        #     self._sys_bins = find_bins_in_path()

        ret = sorted(
            [name for name in self.commands if name.startswith(prefix)]
            # + [name for name in self._sys_bins if name.startswith(prefix)]
        )
        return ret
