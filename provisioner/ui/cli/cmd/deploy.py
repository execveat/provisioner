from __future__ import annotations
from typing import List, TYPE_CHECKING

from pypsi.core import PypsiArgParser, CommandShortCircuit

from provisioner.core.director import CLIDirector
from provisioner.ui.cli.cmd import ProvisionerCommand
if TYPE_CHECKING:
    from provisioner.ui.cli.shell import ProvisionerShell


class DeployCommand(ProvisionerCommand):
    """
    Deploy the current configured job
    """

    def __init__(self, name='deploy', **kwargs):
        self.parser = PypsiArgParser(
            prog=name,
            description="Deploy the current configured job",
        )
        self.parser.add_argument(
            '-c', '--check', help='Check that the job is valid without actually deploying',
            action='store_true', default=False,
        )
        self.parser.add_argument(
            '-d', '--debug', help='Allow exceptions to bubble up for debugging',
            action='store_true', default=False,
        )

        super().__init__(name=name, brief="deploys the current job against configured targets", **kwargs)

    # pylint: disable=arguments-differ
    def run(self, shell: ProvisionerShell, raw_args: List[str]) -> int:
        """
        Parse args and perform appropriate action
        """
        try:
            args = self.parser.parse_args(raw_args)
        except CommandShortCircuit as e:
            shell.error(e)
            return 1

        # todo implment job check #121
        # errors = shell.ctx.job.check()

        # if errors:
        #     for plug, err in errors.items():
        #         shell.error(f"{plug}: {err}")
        #     return 1

        if args.check:  # User only asked for dry run, return regardless of job_good
            print("Current job returns all good :)")
            return 0

        try:
            director = CLIDirector(shell=shell, plugin_store=shell.ctx.plugin_store, debug=args.debug)
            director.execute_job()
        except Exception as exc:
            if args.debug:
                raise
            shell.error(exc)
            shell.error("Error occurred while executing the job, cancelling now..")
            return 1

        return 0
