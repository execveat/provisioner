from __future__ import annotations
from typing import List, TYPE_CHECKING

from bson import ObjectId
from pypsi.core import PypsiArgParser, CommandShortCircuit

from provisioner.core.jobs import Job
from provisioner.ui.cli.cmd import ProvisionerCommand
from provisioner.structs.plugins import RecursiveConfig

if TYPE_CHECKING:
    from provisioner.ui.cli.shell import ProvisionerShell


class JobCommand(ProvisionerCommand):
    """
    Manage jobs for the provisioner
    """

    def __init__(self, name='job', **kwargs):
        self.parser = PypsiArgParser(
            prog=name,
            description="Manage the current configured job",
        )
        self.parser.add_argument(
            'action', help='Action to perform - save or load', choices=['save', 'load', 'list', 'delete'],
            completer=lambda _, args, prefix: ['save', 'load', 'list', 'delete']
        )
        self.parser.add_argument('name', nargs='?', help='Name to save job as', completer=self.complete_jobs)

        super().__init__(name=name, brief="manage the current or saved provisioner jobs", **kwargs)

    # pylint: disable=arguments-differ
    def run(self, shell: ProvisionerShell, raw_args: List[str]) -> int:
        """
        Parse args and perform appropriate action
        """
        try:
            args = self.parser.parse_args(raw_args)
        except CommandShortCircuit as e:
            shell.error(e)
            return 1

        if args.action == 'list':
            for job in shell.ctx.db.jobs.find():
                print(job.get("name", 'unknown name'))
            return 0
        elif not args.name:
            shell.error('name argument is required')
            return 2
        elif args.action == 'save':
            return self.save(shell, args.name)
        elif args.action == 'load':
            return self.load(shell, args.name)
        elif args.action == 'delete':
            return self.delete(shell, args.name)

        shell.error(f"unknown subcommand: {args.action}")
        return 3

    def save(self, shell: ProvisionerShell, name: str) -> int:
        """
        Save the current shell's job to the database
        """
        if shell.ctx.db.jobs.find_one({'name': name}):
            shell.error('job already exists')
            return 4

        # We have to call job.json() to serialize the object, then json.loads into a dict to send to mongo
        shell.ctx.db.jobs.insert({**shell.ctx.job.dict(), 'name': name}, check_keys=False)
        return 0

    def delete(self, shell: ProvisionerShell, name: str) -> int:
        """
        Delete the specified job from the database
        """
        if shell.ctx.db.jobs.find_one({'name': name}):
            shell.ctx.db.jobs.delete_one({'name': name})
            return 0

        shell.error(f'job {name} not found, can not remove')
        return 5

    def load(self, shell: ProvisionerShell, name: str) -> int:
        """
        Load a job from the DB into the shell
        """
        data = shell.ctx.db.jobs.find_one({'name': name})
        if not data:
            shell.error('job not found')
            return 5

        # custom validator on job from dict, try to get plugin from pluginstore
        try:
            del data['_id']
            tmp = data['options']['children'][0]
            tmp['plugin_store'] = shell.ctx.plugin_store

            data['options']['children'][0] = RecursiveConfig(**tmp).dict()
            job = Job.parse_obj(data)
        except Exception as e:
            shell.error(f'failed to load job: {e}')
            return 6

        # Generate a new job id
        job.job_id = ObjectId()
        shell.ctx.job = job
        print(f'loaded {name} (new id {job.job_id})')
        return 0
