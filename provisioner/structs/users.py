from dataclasses import dataclass, field
from random import randint

from typing import List, Optional
from provisioner.core.mongodb import MongoDB


@dataclass
class User:
    """
    Dataclass for User. Implements attributes below when initialized with the
    corresponding kwargs.
    """
    username: str
    full_name: Optional[str] = None
    password: Optional[str] = None
    tag: Optional[str] = None
    tags: Optional[List[str]] = None
    groups: Optional[List[str]] = None
    auth: Optional[str] = None
    user_list: Optional[str] = None
    uid: Optional[int] = None
    gid: Optional[int] = None

    @property
    def name(self) -> str:
        """Shorthand for username"""
        return self.username


@dataclass
class UserList:
    """
    Initialize new user list from a file path
    """
    name: str
    db: MongoDB
    users: List[User] = field(default_factory=list)
    previous_users: List[User] = field(default_factory=list)

    def __bool__(self) -> bool:
        """
        Return if the user list is empty or not
        """
        return len(self.users) > 0

    def __iter__(self):
        """
        Allows for: `for user in UserList:`
        """
        return iter(self.users)

    def add_user(self, user: dict):
        """
        Add a user to the <UserList> and perform necessary validation
        """
        content = {}
        content['username'] = user['username']
        content['full_name'] = user.get('full_name', user['username'])
        content['password'] = user.get('password', "")
        content['tag'] = user.get('tag', "")
        content['tags'] = user.get('tags', [])
        content['groups'] = user.get('groups', [])
        content['auth'] = user.get('auth', "")
        content['uid'] = user.get('uid', None)
        content['gid'] = user.get('gid', None)
        content['user_list'] = self.name

        self.users.append(User(**content))

    def search_users(self, **kwargs) -> list:
        """
        Search for users in the DB using MongoDB specific search parameters
        """
        to_search = dict()
        limit = kwargs.pop('limit', 1)
        kwargs['user_list'] = self.name

        # Remove any None/Falsey kwargs
        for prop in kwargs:
            # Use MongoDBs special notation for searching from words in arrays
            if isinstance(kwargs[prop], list):
                to_search[prop] = {'$in': kwargs[prop]}
            elif kwargs[prop]:
                to_search[prop] = kwargs[prop]

        cursor = self.db.db.user_db.users.find(to_search)
        cursor.limit(limit)

        return list(cursor)

    def get_random_user(
            self, no_previous_users: bool = False, username: str = None, tags: List[str] = None,
            groups: List[str] = None, auth: List[str] = None
    ):
        """
        Get a random user from the DB

        Raises:
            ValueError - When it is specified to not get previous users, but all users are previously used
        """
        results: list = list(self.search_users(username=username, tags=tags, groups=groups, auth=auth))
        index = randint(0, len(results) - 1)
        user = results[index]
        if no_previous_users:
            while user in self.previous_users:
                results.pop(index)
                index = randint(0, len(results) - 1)
                user = results[index]

        if user not in self.previous_users:
            self.previous_users.append(user)

        return user
