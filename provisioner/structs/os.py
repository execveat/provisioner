from enum import Enum


class OS(Enum):
    """
    Operating System Enum
    """
    Linux = "linux"
    Ubuntu = "ubuntu"
    Debian = "debian"
    DebianFamily = Debian
    Windows = "windows"
    Windows10 = "windows10"
