from __future__ import annotations

from bson import ObjectId
from pydantic import Extra, BaseConfig, BaseModel as PydanticBaseModel

from provisioner.utils.encoding import JSONDecoder, encode_range_object


class BaseModel(PydanticBaseModel):
    """Customized Pydantic Base Model."""
    class Config(BaseConfig):
        """Pydantic Configuration."""
        # allow_population_by_field_name is required to prevent pydantic from overwriting validation results on aliased
        # fields. Ex, without this, `default_val` is turned into a function during default_is_a_func, then
        # pydantic will overwrite the returned value of default_is_a_func with the original passed value
        allow_population_by_field_name = True
        arbitrary_types_allowed = True
        validate_assignment = True
        use_enum_values = True
        extra = Extra.allow
        # Initialize custom decoder and store decode function in pydantic
        json_loads = JSONDecoder().decode
        # Custom
        json_encoders = {
            range: encode_range_object,
            ObjectId: str
        }
