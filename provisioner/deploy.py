# pylint: disable=redefined-builtin
from __future__ import annotations  # Allow returning class type from class method

import argparse
import logging
import os.path
from pathlib import Path
from typing import Optional, Union

from cincoconfig import Schema

from provisioner.config import PROVISIONER_SCHEMA


class SiteDeploymentSingleton:
    """
    Static Configurations calculated at runtime
    """
    __instance: Optional['SiteDeploymentSingleton'] = None

    def __init__(self, config_path: str = None, config_format: str = None,
                 args: argparse.Namespace = None):
        self.base_path = os.path.dirname(os.path.abspath(__file__))
        self.builtin_path = os.path.join(self.base_path, 'plugins')
        self.parent_path = os.path.normpath(os.path.join(self.base_path, '../'))
        self._config_path = config_path or Path(self.base_path, 'conf.yml')
        self._config_format = config_format or 'yaml'
        self.config: Schema = self.load_config(f_path=self._config_path, format=config_format, args=args)
        self.plugin_path = self.config.plugins_path
        self.logger: logging.Logger = self.setup_logger()

    def __call__(self) -> SiteDeploymentSingleton:
        """
        Get plugin store if it's instantiated, otherwise create one and return it
        """
        if not self.__instance:
            SiteDeploymentSingleton.__instance = SiteDeploymentSingleton()
        return SiteDeploymentSingleton.__instance  # type: ignore

    def load_config(self, f_path: Union[str, Path] = None, format: str = None,
                    args: argparse.Namespace = None) -> Schema:
        """
        Load the YAML config file and override with any argparse args passed
        """
        # Initialize the config
        config = PROVISIONER_SCHEMA()
        config.load(str(f_path or self._config_path), format or self._config_format)

        if args:
            config.cmdline_args_override(args)

        return config

    def save_config(self, f_path: Union[str, Path] = None, format: str = "yaml"):
        """
        Save config object to specified file and format
        """
        self.config.save(f_path, format=format)

    def setup_logger(self):
        """
        Configure logger settings to be used by the rest of the project
        """
        logger = logging.getLogger('provisioner')
        logger.setLevel(logging.DEBUG)
        if not logger.handlers:
            formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s", "%Y-%m-%d %H:%M:%S")

            file_handler = logging.FileHandler(self.config.log_file)
            file_handler.setLevel(getattr(logging, self.config.log_level_file.upper(), logging.DEBUG))

            console_handler = logging.StreamHandler()
            console_handler.setLevel(getattr(logging, self.config.log_level_console.upper(), logging.INFO))

            file_handler.setFormatter(formatter)
            console_handler.setFormatter(formatter)

            logger.addHandler(file_handler)
            logger.addHandler(console_handler)

        return logger


SiteDeployment = SiteDeploymentSingleton()
