from pydantic.typing import AnyCallable


def exclude_fields(*fields):
    """Modify exclude kwarg to pydnatic dict() and json() methods to contain additional fields by default."""
    def dec(f: AnyCallable):
        def wrap(*args, **kwargs):
            for field in fields:
                if isinstance(kwargs.get('exclude'), dict) and field not in kwargs['exclude']:
                    kwargs['exclude'][field] = ...
                elif isinstance(kwargs.get('exclude'), set) and field not in kwargs['exclude']:
                    kwargs['exclude'].add(field)
                else:
                    kwargs['exclude'] = {field}
            return f(*args, **kwargs)
        return wrap
    return dec
