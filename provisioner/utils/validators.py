from __future__ import annotations
import re
from typing import List, Union, Dict, TYPE_CHECKING, Optional
from ipaddress import ip_address, IPv4Address, IPv6Address

from bson import ObjectId

if TYPE_CHECKING:
    from provisioner.structs.options import AnyOption


def value_is_object_id(val):
    """Ensure the value is an ObjectID."""
    return ObjectId(val)


def options_are_instantiated(value: List[Union[AnyOption, Dict]]):
    """Convert a list of Dicts to a list of Instantiated Options"""
    from provisioner.structs.options import AnyOption  # pylint: disable=import-outside-toplevel # avoid circular import
    for idx, opt in enumerate(value):
        if isinstance(opt, dict):
            value[idx] = AnyOption(**opt)
    return value


def coerce_oid_from_options(value: List[Union[AnyOption, Dict[str, Dict]]]):
    """
    Pydantic Validator to turn a dictionary mapping of oid: {Option} into a single consumable dictionary. Ex:
      { config_file: {type: str, default: /etc/exports} }
      ->
      { oid: config_file, type: str, default: /etc/exports} }
    :param cls: Class being validated
    :param value: Either a List of Option Dicts (ignored), List of Option Classes (ignored),
                    or a Mapping of {oid: OptionDict}
    :return: List of Option Dicts
    """
    if not isinstance(value, dict):
        return value
    ret = []
    for oid, opt in value.items():
        opt['oid'] = oid
        ret.append(opt)
    return ret


def oid_contains_no_dots(new: str) -> str:
    """Verify the OID does not contain any periods ('.')"""
    if '.' in new:
        raise ValueError("field 'oid' cannot contain '.'")
    return new


def valid_net_address(target: str) -> bool:
    """
    Whether given target is a valid ipv4, ipv6 or hostname
    """
    try:
        t_type = ip_address(target)
        if isinstance(t_type, (IPv4Address, IPv6Address)):
            # target is a valid ipv4 or ipv6 address
            return True
    except ValueError:
        # target is either invalid ipv4/ipv6 or it is a hostname
        pass

    # Test whether target is a valid hostname
    if len(target) > 255:
        return False
    if target[-1] == ".":
        target = target[:-1]  # strip exactly one dot from the right, if present

    allowed = re.compile(r"(?!-)[A-Z\d-]{1,63}(?<!-)$", re.IGNORECASE)
    return all(allowed.match(segment) for segment in target.split("."))


def validate_net_addr(new) -> Optional[str]:
    """Validate if the values are valid network addresses"""
    # todo support choices being a list of CIDRs that a single IP should fit into
    invalid = []
    if new is None:
        return new
    if isinstance(new, (list, set)):
        for target in new:
            if not valid_net_address(target):
                invalid.append(target)
    else:
        if not valid_net_address(new):
            invalid.append(new)

    if invalid:
        raise ValueError(f"The following are not hostname|ipv4|ipv6: {', '.join(invalid)}")

    return new


def values_in_choices(new, values):
    """
    Ensure new values are within valid choices
    :param new: List of items or single new item
    :raises ValueError: When a value is not in valid list of choices
    """
    choices = values.get('choices')
    invalid = []
    if choices:
        for value in new:
            if value not in choices:
                invalid.append(value)
    if invalid:
        raise ValueError(f"'{', '.join(invalid)}' are not valid choices within: {choices}")

    return new
