from pathlib import Path


def shorten_home_path(path: Path) -> Path:
    """
    Shorten home directory if present, if not, no modification.

    :param path: Path object to replace homedir with '~'
    :return: New Path object with tilde '~' if homedir present
    """
    try:
        path.relative_to(Path.home())
        return Path(str(path).replace(str(Path.home()), '~'))
    except ValueError:
        pass  # this happens if not relative to home
    return path
