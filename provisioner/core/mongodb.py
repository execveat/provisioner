from logging import warning

from bson.codec_options import TypeRegistry
from pymongo import MongoClient
from cincoconfig import Schema

from provisioner.structs.pydantic import BaseModel
from provisioner.utils.encoding import RangeCodec


class MongoDB:
    """
    MongoDB connector
    """
    def __init__(self, config):
        self.conf: Schema = config

        # TODO (burrches): setup and configure SSL example
        # https://hub.docker.com/_/mongo
        # https://api.mongodb.com/python/current/api/pymongo/mongo_client.html#pymongo.mongo_client.MongoClient
        self._mongo: MongoClient = MongoClient(
            self.conf.database.host,
            self.conf.database.port,
            username=self.conf.database.user,
            password=self.conf.database.password,
            serverSelectionTimeoutMS=3000,  # 3 seconds
            type_registry=TypeRegistry(type_codecs=[RangeCodec()], fallback_encoder=BaseModel.__json_encoder__)
        )
        # Get database name from config
        self._db_name = self.conf.database.database
        self._mongo.user_db.users.create_index("user_list")

    @property
    def db(self):
        """
        Return Mongo DB object
        """
        return self._mongo[self._db_name]

    @property
    def users(self):
        """
        Return Users collection, where user lists and template users are stored
        """
        return self.db.users

    @property
    def runtime(self):
        """
        Return Runtime collection, where events from the C2games.Opfor Ansible Collection are gathered
        """
        return self.db.runtime

    @property
    def events(self):
        """
        Return Events collection, where ansible Events are gathered
        """
        return self.db.events

    @property
    def event_data(self):
        """
        Return Event Data collection, where information about Events are gathered, such as hosts, jobs, reports, etc
        """
        return self.db.event_data

    @property
    def jobs(self):
        """
        Return Jobs collection, Job configurations are kept
        """
        return self.db.jobs

    def save_deployed_vm(self, vm_config: dict):
        """
        Save off information about the deployed VM to the database
        """
        # In the templates DB, vm_templates collection, insert document
        if vm_config.get('name', None):
            self.db.vm_templates.insert_one(vm_config)
        else:
            warning("VM was not saved, no name given.")

    def find_vm_config(self, vm_name: str) -> dict:
        """
        Return the first match on <vm_name> under the templates DB, and the
        vm_templates collections.
        """
        return self.db.vm_templates.find_one({"name": vm_name})

    def save_userlist(self, users: list, user_list: str, force=False):
        """
        Save <UserList> to the DB and set collection to index from user_list field
        """
        if not force and self.users.find({'user_list': user_list}).count() > 0:
            raise ValueError(f"Userlist '{user_list}' already exists! Use force=true to override")

        for user in users:
            user['user_list'] = user_list

        self.users.insert_many(users)
        self.users.create_index("user_list")
