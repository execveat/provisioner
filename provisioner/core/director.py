from __future__ import annotations

import os
import shutil
import logging
from pathlib import Path
from tempfile import TemporaryDirectory as TmpDir
from typing import TYPE_CHECKING, Dict, List

import yaml
from ansible_runner import RunnerConfig, Runner
from pypsi.ansi import AnsiCodes
from pypsi.shell import Shell

from provisioner.deploy import SiteDeployment
from provisioner.core.jobs import JobStatus
from provisioner.core.mongodb import MongoDB
from provisioner.structs.plugins.store import PluginStore

if TYPE_CHECKING:
    from provisioner.core.jobs import Job
    from provisioner.structs.plugins import RecursiveConfig, Plugin


class TemporaryDirectory(TmpDir):
    """
    Overridden temp directory class to allow for not cleaning up
    """
    def __init__(self, *args, auto_clean=True, **kwargs):
        self._do_cleanup = auto_clean
        super().__init__(*args, **kwargs)

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self._do_cleanup:
            self.cleanup()


class DirectorPaths:
    """
    Container for all paths within the Director generated environment
    """
    def __init__(self, tmp_prefix='c2games_', auto_clean=True):
        self._deployment = TemporaryDirectory(prefix=tmp_prefix, auto_clean=auto_clean)
        self.deployment = Path(self._deployment.name)
        self.env = Path(self.deployment, 'env')
        self.inventory = Path(self.deployment, 'inventory')
        self.project: Path = Path(self.deployment, 'project')
        self.playbook = Path(self.project, 'playbook.yml')
        self.roles = Path(self.project, 'roles')
        self.repo = Path(SiteDeployment.base_path, '..').resolve()
        self.config = Path(self.deployment, 'c2games.cfg.json')

    def cleanup(self):
        """Clean up the temporary directory"""
        self._deployment.cleanup()


class Director:
    """
    Execute jobs using Ansbile runner, and handle the output
    """
    # todo unit tests mang
    def __init__(self, job: Job, database: MongoDB, plugin_store: PluginStore, debug=False):
        """
        Initialize the Director
        :param config: Configuration object from Site Deployment
        :param database: Instantiated Database Object
        """
        self.paths = DirectorPaths(auto_clean=not debug)
        self.roles_copied: List[str] = []
        self.job = job
        self.plugin_store = plugin_store
        self.db = database.db
        self.debug = debug

    def get_event_handler(self):
        """
        Get the event handler function for Ansible.
        :return: Event Handler Callback
        """
        def handler(event):
            event['job_id'] = self.job.job_id
            # Add some special handling around log messages due to Issue #154 duplicate key errors
            if event.get('_id'):
                print(f"Found an '_id' field on this event, renaming to __id. The event was {event}")
                # todo for some reason log output isn't working here, so we just print
                # logging.warning("Found an '_id' field on this event, renaming to __id")
                # logging.debug(f'The event was {event}')
                if event.get('__id'):
                    # logging.warning('WHOAH THERE IS ALREADY AN __id too!')
                    print('WHOAH THERE IS ALREADY AN __id too!')
                event['__id'] = event['_id']
                del event['_id']
            try:
                self.db.events.insert(event, check_keys=False)
            except Exception as e:
                print(f'Failed to insert document: {e}')
        return handler

    def get_status_handler(self):  # pylint: disable=unused-argument
        """
        Get a status handler function for Ansible.
        :return:
        """
        def handler(status, runner_config=None):  # pylint: disable=unused-argument
            print(f"{AnsiCodes.white}Status changed to: {status.get('status')}{AnsiCodes.reset}")
        return handler

    def get_cancel_callback(self):  # pylint: disable=unused-argument
        """
        Get the handler function for cancel event.
        """
        def handler():
            pass
        return handler

    def get_finished_callback(self):  # pylint: disable=unused-argument
        """
        Get the handler function for finished event.
        """
        def handler(runner: Runner):  # pylint: disable=unused-argument
            logging.info('Ansible task complete')
        return handler

    def _generate_subtask(self, config: RecursiveConfig) -> List[Dict]:
        """
        Generate the Ansible Tasks for subconfigurations. These are special because:
        1. They are recursive - multiple levels of sub-configurations can exist.
           These are flattened out into a single list of tasks
        2. The variables are defined on ONLY the task, not the entire play.
           This is to prevent interference between plugins.
        :param config: Configuration to start recurse-ing downward
        :return: List of Ansible Tasks, represented as dictionaries
        """
        ret = []
        for child in config.children:
            if not child.plugin:  # Satisfy MyPy
                raise ValueError(f"Plugin Class not defined on {child.path}")

            # Subslassed persistence modules for instance need to specially craft name/vars
            ret.append(child.plugin.get_usage(
                f"{child.plugin.name} - {child.path}:{child.name}",
                child.tree(by_var=True)
            ))

            if child.children:
                ret.extend(self._generate_subtask(child))
        return ret

    def generate_plays(self, config: RecursiveConfig) -> List[Dict]:
        """
        Generate an Ansible Play for each Primary Plugin Configuration. Sub-configurations will be
        added as individual tasks within the play.
        :param config: Plugin Config
        :return: List of Ansible Plays, represented as dictionaries
        """
        # plays represents the playbook to dump to yaml in temp dir
        plays: List[Dict] = []

        # Sort plugins by altitude before iterating over list
        config.children.sort(key=lambda x: x.option_from_name('altitude').val)  # type: ignore

        # iterate over all children of the plugin config, and generate a play for each
        for child in config.children:
            if not child.plugin:
                raise ValueError(f"Plugin Class not defined on {child.path}")
            # Base play that all everything in playbook is built off. Contains common configuration
            # for all hosts
            play = {
                'hosts': self.job.options.resolve_option('ansible', 'hosts').tree(),
                'user': self.job.options.resolve_option('ansible', 'user').tree(),
                'become': self.job.options.resolve_option('ansible', 'become').tree(),
                'gather_facts': self.job.options.resolve_option('ansible', 'gather_facts').tree(),
                'become_method': self.job.options.resolve_option('ansible', 'become_method').tree(),
                'become_user': self.job.options.resolve_option('ansible', 'become_user').tree(),
                'tasks': [],
                'vars': {
                    # Set the primary configuration options at the play level
                    **child.tree(by_var=True),
                    'ansible_password': self.job.options.resolve_option('ansible', 'password').tree(),
                    # Add the C2Games metadata. This will overwrite anything from chile.tree()
                    'c2games': {
                        'event_id': self.job.options.resolve_option('job', 'event_id').tree(),
                        'config_id': f"{child.path}:{child.name}"
                    },
                },
            }

            # Add a task to include the plugin role
            play['tasks'].append({
                **child.plugin.usage,  # type: ignore
                'name': f"{child.plugin.name} - {child.path}:{child.name}",
            })

            # register the plugin as successfully executing just after it's done, before other tasks
            play['tasks'].append({
                'name': 'Register Success',
                'c2games.opfor.register_plugin': {
                    '_plugin_id': child.plugin.id
                }
            })

            # Add tasks for each subtask. Each of these subtasks will have access to the original configuration,
            # as well as any additional configuration options defined by the subtask.
            # NOTE - subtask configuration values will overwrite primary configuration values by the same ID/Path
            play['tasks'].extend(self._generate_subtask(child))

            # Finally add the play to the list of plays to return
            plays.append(play)
        return plays

    def execute_job(self):
        """
        Execute a job
        """
        # Define paths within temp dir
        cmdline = []
        # environment variables to dump to yaml in temp dir
        env = {
            # AbsolutePATH to provisioner-plugins directory
            "ANSIBLE_ROLES_PATH": str(Path(SiteDeployment.config.plugins_path).resolve()),
            'C2GAMES_CONFIG': str(self.paths.config),
        }

        # generate config file
        with open(self.paths.config, 'w') as f:
            f.write(SiteDeployment.config.dumps('json').decode())

        verbosity: int = self.job.options.resolve_option('ansible', 'verbosity').val  # type: ignore
        if verbosity is not None and verbosity > 0:
            cmdline.append('-{}'.format('v' * verbosity))

        os.mkdir(self.paths.env)
        os.mkdir(self.paths.inventory)
        os.makedirs(self.paths.roles)
        os.environ["ANSIBLE_NO_TARGET_SYSLOG"] = "True"

        ssh_key = self.job.options.resolve_option('ansible', 'ssh_key').val  # type: ignore
        if ssh_key:
            shutil.copy(Path(ssh_key).expanduser(), Path(self.paths.env, 'ssh_key'))

        # todo re-write plugins with supported module names replaced? Ex, user module?
        # todo handle prompt module
        # Generate Plays for Playbook
        plays = self.generate_plays(self.job.options.resolve_config('plugins'))

        # Write Playbook
        with open(self.paths.playbook, 'w') as f:
            yaml.dump(plays, f)

        # Write Inventory
        with open(Path(self.paths.inventory, 'hosts'), 'w') as f:
            f.writelines('\n'.join(self.job.options.resolve_option('job', 'targets').val))  # type: ignore

        # Write CLI args to Ansible
        with open(Path(self.paths.env, 'cmdline'), 'w') as f:
            f.write(' '.join(cmdline))

        # Execute Ansible Runner
        runner_config = RunnerConfig(
            private_data_dir=str(self.paths.deployment),
            playbook='playbook.yml',
            envvars=env,
            quiet=True,
        )
        runner_config.prepare()
        runner = Runner(
            runner_config,
            status_handler=self.get_status_handler(), event_handler=self.get_event_handler(),
            cancel_callback=self.get_cancel_callback(), finished_callback=self.get_finished_callback()
        )

        if self.debug:
            print(f'{AnsiCodes.white}Executing{AnsiCodes.reset} "{" ".join(runner_config.command)}"')

        self.job.status = JobStatus.Running
        runner.run()

        # todo check results --
        # https://ansible-runner.readthedocs.io/en/latest/intro.html#runner-artifacts-directory-hierarchy
        if runner.rc != 0:
            try:
                stdout = runner.stdout.read()
            except Exception as e:
                stdout = f'<{e}>'

            self.job.status = JobStatus.Failed
            raise RuntimeError(f'ansible execution failed with exitcode={runner.rc}: {stdout}')

        print(f'{AnsiCodes.white}Runner successful{AnsiCodes.reset}')
        self.job.status = JobStatus.Successful

        # Clean up if not debugging
        if not self.debug:
            self.paths.cleanup()


class CLIDirector(Director):
    """
    Special director to dump events to command line interface
    """
    def __init__(self, shell: Shell, *args, **kwargs):
        """
        Initialize CLI Director, inherits all options from `Director`
        :param shell: PyPSI Shell object
        :param args:
        :param kwargs:
        """
        self.shell = shell
        # Pull out job and db from the shell object
        kwargs.setdefault('job', shell.ctx.job)
        kwargs.setdefault('database', shell.ctx.db)
        super().__init__(*args, **kwargs)

    def get_event_handler(self):
        """
        Override Event Handler to also print messages to CLI
        :return: Event Handler
        """
        handler = super().get_event_handler()

        def cli_handler(event):
            msg = event.get('stdout')
            if event.get('event') == 'verbose':
                print(f"{AnsiCodes.blue}{msg}{AnsiCodes.reset}")
            elif msg:
                print(f"{msg}")

            handler(event)

        return cli_handler

    def get_finished_callback(self):
        """
        Override Finished Handler to also print messages to CLI
        :return: Event Handler
        """
        handler = super().get_finished_callback()

        def cli_handler(runner):
            print(f'{AnsiCodes.white}Ansible task is complete{AnsiCodes.reset}')

            handler(runner)

        return cli_handler
