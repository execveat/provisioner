import argparse
import sys
from pathlib import Path

import yaml
from xkcdpass import xkcd_password as xp

try:
    from provisioner.db.mongodb import MongoDB
    from provisioner.deploy import SiteDeployment
except ImportError:
    print('usage: python -m provisioner.scripts.import_userlist')
    sys.exit(1)


def import_userlist(filename, name=None, no_pass=False):
    """
    Import user list specified by `filename` into the Database,
    optionally generating passwords.
    :param filename: File to import
    :param name: Name of the Userlist (defaults to stem of filename)
    :param no_pass: Whether to Autogenerate passwords for users
    """
    word_file = xp.locate_wordfile()
    my_words = xp.generate_wordlist(wordfile=word_file, min_length=5, max_length=8)
    db = MongoDB(SiteDeployment().config)
    name = name or Path(filename).stem

    with open(filename) as f:
        users = yaml.load(f, Loader=yaml.SafeLoader)
    if not no_pass:
        for user in users:
            user.setdefault('password', xp.generate_xkcdpassword(my_words, delimiter='.', numwords=2))

    db.save_userlist(users, name)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Import a YAML into the database')
    parser.add_argument('file', help='file to import')
    parser.add_argument('--user-list', '-u', help='name of user list (defaults to filename)')
    parser.add_argument('--no-pass', '-P', action='store_true', help='do not auto-generate passwords')
    args = parser.parse_args()
    import_userlist(args.file, name=args.user_list, no_pass=args.no_pass)
