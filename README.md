# Provisioner
-------------
Automates the deployment of VMs with selected vulnerabilities and misconfigured
services for competitions.


## Installation and requirements
--------------------------------

1. Install dependencies

```sh
apt install python3 python3-venv mongodb
```

*NOTE* A python version of 3.7 or 3.8 is recommended

2. Install Ansible and PyMongo systemwide

```sh
sudo pip3 install ansible pymongo~=3.0
```

3. Install c2games ansible galaxy

```sh
ansible-galaxy collection install c2games.opfor
```

4. Clone Repository

```sh
git clone git@gitlab.com:c2-games/red-team/provisioner.git
cd provisioner
```

5. Initialize Virtual Environment

```sh
python3 -m venv venv
source venv/bin/activate
```

6. Upgrade pip

```
pip install -U pip
```

7. Install project dependencies

```
pip install -r requirements.txt -r requirements-dev.txt
```

### Running in Docker
---------------------

1. Override the following variables if required
    - `C2GAMES_PROVISIONER_PLUGIN_PATH` -- defaults to `$PWD/../provisioner-plugins`
    - `C2GAMES_ANSIBLE_KEY` -- defaults to `~/.ssh/ansible_key`
    - `C2GAMES_PROVISIONER_CONFIG` -- defaults to `$PWD/provisioner/conf.yml`

2. Build & Start environment

```sh
docker-compose up --build -d
```

3. Enter provisioner CLI

```sh
docker-compose exec provisioner provisioner
```

## Getting Started Guide
------------------------

The provisioner currently has a fully tab-completable command line interface. A Web UI is
planned in future work. The command line interface can be started by running the following:

```bash
python provisioner/cli.py
```


If you find the `cli.py` script not giving you a prompt, that is caused by an inability to
communicate to a local mongodb instance. Install it, or run mongo via docker and try again.


When `cli.py` is run, recursive scan of the `plugins_path` is performed. The `plugins_path`
can be configured in `provisioner/conf.yml`. Example output would like like the following:

```
$ python provisioner/cli.py
loaded 0 plugin(s)
loaded 1 plugin(s)
loaded 31 plugin(s)
loaded 0 plugin(s)
[17:26:08] [default_event] provisioner )>
```


#### Deploy a plugin

1. Configure the host(s) to deploy to

`set job!targets 192.1.1.1`

2. Select the plugins you wish to run against a host(s). Plugins and commands are tab completable,
so typing `use ` and then tabbing, will list the valid plugins available.

To use a plugin, run the `use` command.

```
> use add_user
plugin now in use at: plugins.add_user:0
```

The name, difficulty, and description of loaded plugins can be viewed by running `show plugins`.

You can choose to use several of the same plugins as well. Each will get their own configuration ID associated with them.

```
[19:13:26] [default_event] provisioner )> use add_user
plugin now in use at: plugins.add_user:1
[19:13:26] [default_event] provisioner )> use add_user
plugin now in use at: plugins.add_user:2
```

3. Configure any job or plugin options

After you have loaded an option, you can configure any options it defines. Below, we will change the username separately
for each configuration ID. Note: you can tab complete options as well.

```
> set plugins.add_user:0!
plugins.add_user:0!group     plugins.add_user:0!limit     plugins.add_user:0!tag       plugins.add_user:0!userlist  plugins.add_user:0!username

> set plugins.add_user:0!username test
> set plugins.add_user:1!username admin
> set plugins.add_user:2!username user
```

4. Execute the plugins

Running the `deploy` command will first run some checks that everything is configured that needs to be, and then will execute
the specified plugins against the target list. The running ansible log will be displayed in the output at the configured verbosity
level. For debugging, try setting `ansible!verbosity` to 2 or more.


##### Optional, but helpful test setup

A test environment can be setup using vagrant. For more information see
[getting-started-with-vagrant](https://www.ncaecybergames.org/getting-started-with-vagrant/)

## Dev testing

#### Mongo DB

For developers, testing changes to the mongo DB driver can be done
by utilizing the upstream mongo docker image instead of installing mongodb.
To run mongo in docker:

``` bash
docker run -p 27017:27017 -d mongo
```

This runs MongoDB and forwards the appropriate port to the local system. Set the MongoDB Address to `localhost` in the configuration file.

NOTE that this is necessary, and will not work, if MongoDB not is installed and running on the local system.
