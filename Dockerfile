FROM python:3.8.10-alpine

RUN mkdir -p /data/provisioner

WORKDIR /data/provisioner

RUN echo -e "[defaults]\ninterpreter_python=$(which python3)" > /etc/ansible.cfg
RUN echo "host_key_checking = False" >> /etc/ansible.cfg
RUN echo -e "stdout_callback = yaml\nstderr_callback = yaml" >> /etc/ansible.cfg

RUN apk update -U &&\
      apk add --no-cache openssh-client gcc

RUN apk add --no-cache --virtual .build-deps musl-dev libffi-dev

COPY requirements.txt .

RUN pip3 install --upgrade pip &&\
      pip3 install -r ./requirements.txt &&\
      pip3 install setuptools pymongo~=3.0 ansible

RUN apk del .build-deps

RUN ansible-galaxy collection install c2games.opfor

# Quick open provisioner
RUN echo -e "#!/bin/sh\npython3 provisioner/cli.py" > /bin/pvs
RUN chmod u+x /bin/pvs
RUN ln -s /bin/pvs /bin/provisioner

# Keep container open
RUN echo -e '#!/bin/sh\nwhile true; do sleep 2; done' > /init
RUN chmod u+x /init

COPY . /data/provisioner/

CMD ["/init"]
